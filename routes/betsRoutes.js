const { Router } = require("express");
const router = Router();
const { BetsPostSchema } = require("../middlewares/schema.bets.middleware");
const { TokenValidation } = require("../middlewares/token.middleware");
const BetService = require("../services/betService");
router.post("/", TokenValidation, BetsPostSchema, (req, res, next) => {
	BetService.create(req.tokenPayload.id, req.body)
		.then((result) => {
			return res.send({
				...result,
			});
		})
		.catch((err) => {
			res.err = err;
			next();
		});
});
module.exports = router;
