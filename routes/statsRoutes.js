const { Router } = require("express");
const router = Router();
const {
	TokenValidationAdmin,
	TokenValidation,
} = require("../middlewares/token.middleware");
const { getStats } = require("../helpers/statsHelper");
const StatsService = require("../services/statsService");

router.get("/", TokenValidation, TokenValidationAdmin, (req, res, next) => {
	StatsService.getAll()
		.then((result) => {
			res.send(result);
		})
		.catch((err) => {
			res.err = err;
			next();
		});
});
module.exports = router;
