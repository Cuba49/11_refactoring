const { Router } = require("express");
const router = Router();
const {
	UserGetSchema,
	UserPostSchema,
	UserPutSchema,
} = require("../middlewares/schema.user.middleware");
const {
	TokenValidationMismatch,
	TokenValidation,
} = require("../middlewares/token.middleware");

const UserService = require("../services/userService");
router.put(
	"/:id",
	TokenValidation,
	TokenValidationMismatch,
	UserPutSchema,
	(req, res, next) => {
		UserService.updateById(req.params.id, req.body)
			.then((result) => {
				return res.send({
					...result,
				});
			})
			.catch((err) => {
				res.err = err;
				next();
			});
	}
);
router.get("/:id", UserGetSchema, (req, res, next) => {
	UserService.getById(req.params.id)
		.then((result) => {
			return res.send({ ...result });
		})
		.catch((err) => {
			res.err = err;
			next();
		});
});
router.post("/", UserPostSchema, (req, res, next) => {
	UserService.create(req.body)
		.then((result) => {
			return res.send({ ...result });
		})
		.catch((err) => {
			res.err = err;
			next();
		});
});
module.exports = router;
