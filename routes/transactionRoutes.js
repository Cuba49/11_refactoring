const { Router } = require("express");
const router = Router();
const {
	TransactionsPostSchema,
} = require("../middlewares/schema.transaction.middleware");
const {
	TokenValidationAdmin,
	TokenValidation,
} = require("../middlewares/token.middleware");
const TransactionService = require("../services/transactionService");
router.post(
	"/",
	TokenValidation,
	TokenValidationAdmin,
	TransactionsPostSchema,
	(req, res, next) => {
		TransactionService.create(req.body)
			.then((result) => {
				return res.send({
					...result,
				});
			})
			.catch((err) => {
				res.err = err;
				next();
			});
	}
);
module.exports = router;
