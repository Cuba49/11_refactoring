const healthRoutes = require("./healthRoutes");
const userRoutes = require("./userRoutes");
const transactionRoutes = require("./transactionRoutes");
const betsRoutes = require("./betsRoutes");
const eventsRoutes = require("./eventsRoutes");
const statsRoutes = require("./statsRoutes");

module.exports = (app) => {
	app.use("/health", healthRoutes);
	app.use("/users", userRoutes);
	app.use("/transactions", transactionRoutes);
	app.use("/bets", betsRoutes);
	app.use("/events", eventsRoutes);
	app.use("/stats", statsRoutes);
};
