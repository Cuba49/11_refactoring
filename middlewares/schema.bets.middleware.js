const joi = require("joi");
const { validateSchemaHelper } = require("../helpers/schemaHelper");
const BetsPostSchema = (req, res, next) => {
	const schema = joi
		.object({
			id: joi.string().uuid(),
			eventId: joi.string().uuid().required(),
			betAmount: joi.number().min(1).required(),
			prediction: joi.string().valid("w1", "w2", "x").required(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};
exports.BetsPostSchema = BetsPostSchema;
