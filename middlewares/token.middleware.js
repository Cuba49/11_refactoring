const jwt = require("jsonwebtoken");
const TokenValidation = (req, res, next) => {
	const token = req.headers["authorization"];
	if (!token) {
		return res.status(401).send({ error: "Not Authorized" });
	}
	try {
		const tokenPayload = jwt.verify(
			token.replace("Bearer ", ""),
			process.env.JWT_SECRET
		);
		req.tokenPayload = tokenPayload;
	} catch (err) {
		return res.status(401).send({ error: "Not Authorized" });
	}
	next();
};
const TokenValidationAdmin = (req, res, next) => {
	if (req.tokenPayload.type != "admin") {
		return res.status(401).send({ error: "Not Authorized" });
	}
	next();
};
const TokenValidationMismatch = (req, res, next) => {
	if (req.params.id !== req.tokenPayload.id) {
		return res.status(401).send({ error: "UserId mismatch" });
	}
	next();
};
exports.TokenValidation = TokenValidation;
exports.TokenValidationAdmin = TokenValidationAdmin;
exports.TokenValidationMismatch = TokenValidationMismatch;
