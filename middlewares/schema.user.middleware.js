const joi = require("joi");
const { validateSchemaHelper } = require("../helpers/schemaHelper");

const UserGetSchema = (req, res, next) => {
	const schema = joi
		.object({
			id: joi.string().uuid(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.params);
};

const UserPutSchema = (req, res, next) => {
	const schema = joi
		.object({
			email: joi.string().email(),
			phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
			name: joi.string(),
			city: joi.string(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};

const UserPostSchema = (req, res, next) => {
	const schema = joi
		.object({
			id: joi.string().uuid(),
			type: joi.string().required(),
			email: joi.string().email().required(),
			phone: joi
				.string()
				.pattern(/^\+?3?8?(0\d{9})$/)
				.required(),
			name: joi.string().required(),
			city: joi.string(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};

exports.UserGetSchema = UserGetSchema;
exports.UserPostSchema = UserPostSchema;
exports.UserPutSchema = UserPutSchema;
