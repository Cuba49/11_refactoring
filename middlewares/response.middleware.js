const responseMiddleware = (req, res, next) => {
	if (res.err) {
		console.log(res.err);
		if (res.err.name === "MyError") {
			return res.status(res.err.code).send({
				error: res.err.message,
			});
		}
		if (res.err.code == "23505") {
			return res.status(400).send({
				error: res.err.detail,
			});
		}
		return res.status(500).send("Internal Server Error");
	}
	next();
};

exports.responseMiddleware = responseMiddleware;
