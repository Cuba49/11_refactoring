const joi = require("joi");
const { validateSchemaHelper } = require("../helpers/schemaHelper");
const TransactionsPostSchema = (req, res, next) => {
	const schema = joi
		.object({
			id: joi.string().uuid(),
			userId: joi.string().uuid().required(),
			cardNumber: joi.string().required(),
			amount: joi.number().min(0).required(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};
exports.TransactionsPostSchema = TransactionsPostSchema;
