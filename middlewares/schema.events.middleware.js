const joi = require("joi");
const { validateSchemaHelper } = require("../helpers/schemaHelper");
const EventsPostSchema = (req, res, next) => {
	const schema = joi
		.object({
			id: joi.string().uuid(),
			type: joi.string().required(),
			homeTeam: joi.string().required(),
			awayTeam: joi.string().required(),
			startAt: joi.date().required(),
			odds: joi
				.object({
					homeWin: joi.number().min(1.01).required(),
					awayWin: joi.number().min(1.01).required(),
					draw: joi.number().min(1.01).required(),
				})
				.required(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};
const EventsPutSchema = (req, res, next) => {
	const schema = joi
		.object({
			score: joi.string().required(),
		})
		.required();
	validateSchemaHelper(res, next, schema, req.body);
};
exports.EventsPostSchema = EventsPostSchema;
exports.EventsPutSchema = EventsPutSchema;
