const dbConfig = require("../knexfile");
const knex = require("knex");
const db = knex(dbConfig.development);
const dbConnectHelper = (uselessRequest, uselessResponse, neededNext) => {
	db.raw("select 1+1 as result")
		.then(function () {
			neededNext();
		})
		.catch(() => {
			throw new Error("No db connection");
		});
};
module.exports = { dbConnectHelper, db };
