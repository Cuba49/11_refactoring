const validateSchemaHelper = (res, next, schema, payload) => {
	const isValidResult = schema.validate(payload);
	if (isValidResult.error) {
		return res.status(400).send({
			error: isValidResult.error.details[0].message,
		});
	} else {
		next();
	}
};
exports.validateSchemaHelper = validateSchemaHelper;
