const undercodeToUpper = (body, list) => {
	list.forEach((whatakey) => {
		const index = whatakey.indexOf("_");
		let newKey = whatakey.replace("_", "");
		newKey = newKey.split("");
		newKey[index] = newKey[index].toUpperCase();
		newKey = newKey.join("");
		body[newKey] = body[whatakey];
		delete body[whatakey];
	});
	return body;
};
const upperToUndercode = (body, list) => {
	list.forEach((whatakey) => {
		const char = findUpperChar(whatakey);
		let newKey = whatakey.replace(char, "_" + char.toLowerCase());
		body[newKey] = body[whatakey];
		delete body[whatakey];
	});
	return body;
};
const findUpperChar = (name) => {
	for (var i = 0; i < name.length; i++) {
		const char = name.charAt(i);
		if (char === char.toUpperCase()) {
			return char;
		}
	}
};
module.exports = { undercodeToUpper, upperToUndercode };
