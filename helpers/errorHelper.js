function MyError(code, message) {
	this.code = code || 404;
	this.name = "MyError";
	this.message = message || "Something wrong";
	this.stack = new Error().stack;
}
MyError.prototype = Object.create(Error.prototype);
MyError.prototype.constructor = MyError;
module.exports = { MyError };
