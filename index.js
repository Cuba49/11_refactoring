const express = require("express");
const app = express();
const { responseMiddleware } = require("./middlewares/response.middleware");
const routes = require("./routes/index");
const port = 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
routes(app);
app.use(responseMiddleware);

app.listen(port, () => {
	console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
