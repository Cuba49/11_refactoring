const { db } = require("../helpers/dbConnectHelper");
class OddsRepository {
	getById(id) {
		return db("odds")
			.where("id", id)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	create(body) {
		return db("odds")
			.insert(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
}
module.exports = new OddsRepository();
