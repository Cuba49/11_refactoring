const { db } = require("../helpers/dbConnectHelper");
class EventRepository {
	updateById(id, body) {
		return db("event")
			.where("id", id)
			.update(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	getById(id) {
		return db("event")
			.where("id", id)
			.then(([result]) => {
				return result;
			});
	}
	create(body) {
		return db("event")
			.insert(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
}
module.exports = new EventRepository();
