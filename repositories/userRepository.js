const { db } = require("../helpers/dbConnectHelper");
class UserRepository {
	updateById(id, body) {
		return db("user")
			.where("id", id)
			.update(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	getById(id) {
		return db("user")
			.where("id", id)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	create(body) {
		return db("user")
			.insert(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	getAll() {
		return db
			.select()
			.table("user")
			.then((users) => {
				return users;
			});
	}
}
module.exports = new UserRepository();
