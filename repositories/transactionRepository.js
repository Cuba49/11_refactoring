const { db } = require("../helpers/dbConnectHelper");
class TransactionRepository {
	create(body) {
		return db("transaction")
			.insert(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
}
module.exports = new TransactionRepository();
