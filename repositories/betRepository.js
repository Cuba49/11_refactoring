const { db } = require("../helpers/dbConnectHelper");
class BetRepository {
	getById(id) {
		return db("bet")
			.where("id", id)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	getByEventId(id) {
		return db("bet")
			.where("event_id", id)
			.andWhere("win", null)
			.then((bets) => {
				return bets;
			});
	}
	create(body) {
		return db("bet")
			.insert(body)
			.returning("*")
			.then(([result]) => {
				return result;
			});
	}
	updateById(id, body) {
		return db("bet")
			.where("id", id)
			.update(body)
			.then(([result]) => {
				return result;
			});
	}
}
module.exports = new BetRepository();
