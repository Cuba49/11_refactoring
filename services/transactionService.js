const { MyError } = require("../helpers/errorHelper");
const UserRepository = require("../repositories/userRepository");
const TransactionRepository = require("../repositories/transactionRepository");
const {
	undercodeToUpper,
	upperToUndercode,
} = require("../helpers/renameHelper");
class TransactionService {
	async create(body) {
		const user = await UserRepository.getById(body.userId);
		if (!user) {
			throw new MyError(400, "User does not exist");
		}
		upperToUndercode(body, ["cardNumber", "userId"]);
		const transaction = await TransactionRepository.create(body);
		const currentBalance = body.amount + user.balance;
		await UserRepository.updateById(body.user_id, {
			balance: currentBalance,
		});
		undercodeToUpper(transaction, [
			"user_id",
			"card_number",
			"created_at",
			"updated_at",
		]);
		return { ...transaction, currentBalance };
	}
}
module.exports = new TransactionService();
