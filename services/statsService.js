const { getStats } = require("../helpers/statsHelper");
class StatsService {
	async getAll() {
		return await getStats();
	}
}
module.exports = new StatsService();
