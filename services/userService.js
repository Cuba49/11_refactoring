const UserRepository = require("../repositories/userRepository");
const jwt = require("jsonwebtoken");
const { statEmitter } = require("../helpers/statsHelper");
const { MyError } = require("../helpers/errorHelper");
const { undercodeToUpper } = require("../helpers/renameHelper");
class UserService {
	updateById(id, body) {
		return UserRepository.updateById(id, body);
	}
	async getById(id) {
		const user = await UserRepository.getById(id);
		if (!user) {
			throw new MyError(404, "User not found");
		}
		return user;
	}
	async create(body) {
		body.balance = 0;
		const user = await UserRepository.create(body);
		undercodeToUpper(user, ["created_at", "updated_at"]);
		statEmitter.emit("newUser");
		return {
			...user,
			accessToken: jwt.sign(
				{ id: user.id, type: user.type },
				process.env.JWT_SECRET
			),
		};
	}
}
module.exports = new UserService();
