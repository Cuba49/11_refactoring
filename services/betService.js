const { statEmitter } = require("../helpers/statsHelper");
const { MyError } = require("../helpers/errorHelper");
const {
	undercodeToUpper,
	upperToUndercode,
} = require("../helpers/renameHelper");
const EventRepository = require("../repositories/eventRepository");
const OddsRepository = require("../repositories/oddsRepository");
const BetRepository = require("../repositories/betRepository");
const UserRepository = require("../repositories/userRepository");
class BetService {
	async create(userId, body) {
		upperToUndercode(body, ["eventId", "betAmount"]);
		body.user_id = userId;
		const user = await UserRepository.getById(userId);
		if (!user) {
			throw new MyError(400, "User does not exist");
		}
		if (+user.balance < +body.bet_amount) {
			throw new MyError(400, "Not enough balance");
		}
		const event = await EventRepository.getById(body.event_id);
		if (!event) {
			throw new MyError(404, "Event not found");
		}
		const odds = await OddsRepository.getById(event.odds_id);
		if (!odds) {
			throw new MyError(404, "Odds not found");
		}
		let multiplier;
		switch (body.prediction) {
			case "w1":
				multiplier = odds.home_win;
				break;
			case "w2":
				multiplier = odds.away_win;
				break;
			case "x":
				multiplier = odds.draw;
				break;
		}
		const bet = await BetRepository.create({
			...body,
			multiplier,
			event_id: event.id,
		});
		const currentBalance = user.balance - body.bet_amount;
		await UserRepository.updateById(userId, {
			balance: currentBalance,
		});
		statEmitter.emit("newBet");
		undercodeToUpper(bet, [
			"bet_amount",
			"event_id",
			"away_team",
			"home_team",
			"odds_id",
			"start_at",
			"updated_at",
			"created_at",
			"user_id",
		]);
		return {
			...bet,
			currentBalance,
		};
	}
}
module.exports = new BetService();
