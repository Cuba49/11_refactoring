const BetRepository = require("../repositories/betRepository");
const EventRepository = require("../repositories/eventRepository");
const {
	undercodeToUpper,
	upperToUndercode,
} = require("../helpers/renameHelper");
const UserRepository = require("../repositories/userRepository");
const { MyError } = require("../helpers/errorHelper");
const OddsRepository = require("../repositories/oddsRepository");
class EventService {
	async create(body) {
		upperToUndercode(body.odds, ["homeWin", "awayWin"]);
		const odds = await OddsRepository.create(body.odds);
		delete body.odds;
		upperToUndercode(body, ["awayTeam", "homeTeam", "startAt"]);
		const event = await EventRepository.create({
			...body,
			odds_id: odds.id,
		});
		statEmitter.emit("newEvent");
		undercodeToUpper(event, [
			"bet_amount",
			"event_id",
			"away_team",
			"home_team",
			"odds_id",
			"start_at",
			"updated_at",
			"created_at",
		]);
		undercodeToUpper(odds, [
			"home_win",
			"away_win",
			"created_at",
			"updated_at",
		]);
		return {
			...event,
			odds,
		};
	}
	async updateById(eventId, body) {
		const bets = await BetRepository.getByEventId(eventId);
		const [w1, w2] = body.score.split(":");
		let result;
		if (+w1 > +w2) {
			result = "w1";
		} else if (+w2 > +w1) {
			result = "w2";
		} else {
			result = "x";
		}
		const event = await EventRepository.updateById(eventId, {
			score: body.score,
		});
		bets.forEach((bet) => updateBet(bet, result));
		undercodeToUpper(event, [
			"bet_amount",
			"event_id",
			"away_team",
			"home_team",
			"odds_id",
			"start_at",
			"updated_at",
			"created_at",
		]);
		return event;
	}
	async updateBet(bet, result) {
		if (bet.prediction == result) {
			await BetRepository.updateById(bet.id, { win: true });
			const user = await UserRepository.getById(bet.user_id);
			return await UserRepository.updateById(bet.user_id, {
				balance: user.balance + bet.bet_amount * bet.multiplier,
			});
		} else {
			return await BetRepository.updateById(bet.id, {
				win: false,
			});
		}
	}
}
module.exports = new EventService();
